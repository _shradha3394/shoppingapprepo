﻿using System;
using System.Collections.Generic;

namespace ShopperPlace.Models
{
    public partial class Type
    {
        public Type()
        {
            Product = new HashSet<Product>();
            TypeCategory = new HashSet<TypeCategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<TypeCategory> TypeCategory { get; set; }
    }
}
