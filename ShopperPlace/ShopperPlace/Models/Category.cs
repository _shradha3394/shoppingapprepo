﻿using System;
using System.Collections.Generic;

namespace ShopperPlace.Models
{
    public partial class Category
    {
        public Category()
        {
            Product = new HashSet<Product>();
            SubCategory = new HashSet<SubCategory>();
            TypeCategory = new HashSet<TypeCategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<SubCategory> SubCategory { get; set; }
        public virtual ICollection<TypeCategory> TypeCategory { get; set; }
    }
}
