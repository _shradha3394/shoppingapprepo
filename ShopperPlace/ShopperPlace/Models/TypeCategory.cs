﻿using System;
using System.Collections.Generic;

namespace ShopperPlace.Models
{
    public partial class TypeCategory
    {
        public int TypeId { get; set; }
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Type Type { get; set; }
    }
}
