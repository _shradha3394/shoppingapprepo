﻿using System;
using System.Collections.Generic;

namespace ShopperPlace.Models
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Quantity { get; set; }
        public decimal? Price { get; set; }
        public int? CategoryId { get; set; }
        public int? BrandId { get; set; }
        public int? SizeId { get; set; }
        public int? TypeId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Category Category { get; set; }
        public virtual Size Size { get; set; }
        public virtual Type Type { get; set; }
    }
}
