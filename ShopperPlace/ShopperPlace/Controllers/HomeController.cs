﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShopperPlace.Models;

namespace ShopperPlace.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ShoppingContext _context;

        public HomeController(ShoppingContext context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public IEnumerable<Type> GetMenuData()
        {
            return this._context.Type
                .Include(item => item.TypeCategory).ThenInclude(item => item.Category).ThenInclude(item=> item.SubCategory)
                .ToList();
        }
    }
}