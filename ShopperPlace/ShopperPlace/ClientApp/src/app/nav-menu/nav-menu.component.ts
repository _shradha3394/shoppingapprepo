import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  childMenuShow = false;
  typeList: any;
  subMenu: any;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get(baseUrl + 'api/Home/GetMenuData').subscribe(result => {
      this.typeList = result;
    }, error => console.error(error));
  }

  collapse() {
    this.isExpanded = false;
  }
  expendMenu(typeId) {

    this.childMenuShow = true;
    this.subMenu = this.typeList.filter(x => x.id === typeId);
    this.subMenu = this.subMenu[0].typeCategory;
  }
  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
